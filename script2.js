function romanoParaNum(str1) {

	if(str1 == null){
		alert("Digite um número válido em maiúsculo")
	};
	var num = char_to_int(str1.charAt(0));
	var pre, curr;

	for(var i = 1; i < str1.length; i++){
		curr = char_to_int(str1.charAt(i));
		pre = char_to_int(str1.charAt(i-1));
		if(curr <= pre){
			num += curr;
		} else {
			num = num - pre*2 + curr;
		}
	}
	alert("O resultado da conversão é: "+ num)
	return num;



}

function char_to_int(c){
	switch (c){
		case 'I': return 1;
		case 'V': return 5;
		case 'X': return 10;
		case 'L': return 50;
		case 'C': return 100;
		case 'D': return 500;
		case 'M': return 1000;
		case 'i': return 1;
		case 'v': return 5;
		case 'x': return 10;
		case 'l': return 50;
		case 'c': return 100;
		case 'd': return 500;
		case 'm': return 1000;
		default: return -1;
	}

}
