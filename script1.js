var casas_unidades = ["", "I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX"];
var casas_dezenas = ["", "X", "XX", "XXX", "XL", "L", "LX", "LXX", "LXXX", "XC"];
var casas_centenas = ["", "C", "CC", "CCC", "CD", "D", "DC", "DCC", "DCCC", "CM"];
var casas_milhares = ["", "M", "MM", "MMM"];

function checarNumero(numero){
	if((parseInt(numero) < 4000) && (parseInt(numero) > 0)){
		var numeral = criarNumero(numero);
		if(numeral.indexOf('undefined') == -1){
			alert("O resultado da conversão é: "+ numeral)

			// window.document.form.numeral.value = numeral;
		}
	}else{
		alert('Digite um número válido entre 1 e 3999');
	}
}


function criarNumero(num){
	var new_num = num;
	var milhares = Math.floor(new_num / 1000);
	new_num -= milhares * 1000;
	var centenas = Math.floor(new_num / 100);
	new_num -= centenas * 100;
	var dezenas = Math.floor(new_num / 10);
	new_num -= dezenas * 10;
	var unidades = Math.floor(new_num / 1);

	if((milhares == NaN)||(centenas == NaN)||(dezenas == NaN)||(unidades == NaN)){
		alert('Insira um número válido.');
	}else{
		var array = new Array(milhares,centenas,dezenas,unidades);
		return montarNumero(array);
	}
}


function montarNumero(posicao){
	var numeral = "";
	numeral += casas_milhares[posicao[0]];
	numeral += casas_centenas[posicao[1]];
	numeral += casas_dezenas[posicao[2]];
	numeral += casas_unidades[posicao[3]];
	return numeral;
}